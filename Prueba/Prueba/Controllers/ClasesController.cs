﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelo;
using Modelo.Entidades;

namespace Prueba.Controllers
{
    public class ClasesController : Controller
    {
        // GET: Clases
        public ActionResult Index()
        {
            var Lista = new List<Tbl_Clases>();
            using (var ctx = new Contexto())
            {
                Lista = ctx.Tbl_Clases.ToList();
            }
            return View(Lista);
        }

        public ActionResult Agregar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Agregar(Tbl_Clases NuevaClase)
        {
            using (var ctx = new Contexto())
            {
                ctx.Tbl_Clases.Add(NuevaClase);
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Editar(int id)
        {
            using (var ctx = new Contexto())
            {
                var clase = ctx.Tbl_Clases.Find(id);
                return View(clase);
            }
        }

        [HttpPost]
        public ActionResult Editar(Tbl_Clases ClaseEditada)
        {
            using (var ctx = new Contexto())
            {
                var clase = ctx.Tbl_Clases.Find(ClaseEditada.Id);
                clase.Nombre = ClaseEditada.Nombre;                
                clase.Profesor = ClaseEditada.Profesor;                
                clase.Grado = ClaseEditada.Grado;                
                clase.Capacidad = ClaseEditada.Capacidad;                
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Eliminar(int id)
        {
            using (var ctx = new Contexto())
            {
                var clase = ctx.Tbl_Clases.Find(id);
                return View(clase);
            }
        }

        [HttpPost]
        public ActionResult Eliminar(Tbl_Clases ClaseEditada)
        {
            using (var ctx = new Contexto())
            {
                var clase = ctx.Tbl_Clases.Find(ClaseEditada.Id);
                ctx.Tbl_Clases.Remove(clase);
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }

    }
}