﻿angular.module('suc-app', ['ngRoute', 'ngMaterial', 'ngMessages', 'angularMoment', 'md.data.table', 'md-steppers', 'ngCookies', 'naif.base64', 'ngBase64Download'])
    .config(function ($mdThemingProvider) {


        // Extienda el tema rojo con un color diferente y haga que el color de contraste sea negro en lugar de blanco.
        // Por ejemplo: el texto del botón resaltado será negro en lugar de blanco.
        //var ekomercio = $mdThemingProvider.extendPalette('blue', {
        //    '50': 'F1F3F4',
        //    '100': 'CFD5DB',
        //    '200': '9CA9B6',
        //    '300': '617283',
        //    '400': '4C637D',
        //    '500': '344C67',
        //    '600': '203954',
        //    '700': '1F3146',
        //    '800': '0F253D',
        //    '900': '0F253D',
        //    'A100': '7D90A3',
        //    'A200': '375169',
        //    'A400': '233E5B',
        //    'A700': '14263C',
        //    'contrastDefaultColor': 'light',

        //    'contrastDarkColors': ['50', '100', //hues qué contraste debe ser 'oscuro' por defecto
        //        '200', '300', 'A100', 'A200'],
        //    'contrastLightColors': undefined
        //});

        // Registre el nuevo mapa de la paleta de colores con el nombre <code> neonRed </ code>
        /*$mdThemingProvider*/.definePalette('ekomercio', ekomercio);

        // Usa ese tema para las intenciones primarias

        //$mdThemingProvider.theme('default')
        //    .primaryPalette('ekomercio', {
        //        'default': '400',
        //        'hue-1': '200',
        //        'hue-2': '600',
        //        'hue-3': 'A100'
        //    })
        //    .accentPalette('blue-grey', {
        //        'default': '100'
        //    })
        //    .warnPalette('red', {
        //        'default': '400',

        //    })
        //    .backgroundPalette('grey', {
        //        'default': '200'
        //    });
    })