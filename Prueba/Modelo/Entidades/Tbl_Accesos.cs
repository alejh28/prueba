﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public class Tbl_Accesos
    {
        [Key]
        public int id { get; set; }
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
        public bool Estado { get; set; }
        public string Perfil { get; set; }
    }
}
