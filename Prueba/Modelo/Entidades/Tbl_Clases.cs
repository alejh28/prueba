﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public class Tbl_Clases
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Profesor { get; set; }
        public string Aula { get; set; }
        public int Grado { get; set; }
        public int Capacidad { get; set; }

    }
}
