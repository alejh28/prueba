﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class Contexto : DbContext
    {
        public Contexto() : base()
        {
            this.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["Prueba"].ConnectionString;

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Tbl_Accesos> Tbl_Accesos { get; set; }

        public DbSet<Tbl_Alumnos> Tbl_Alumnos { get; set; }

        public DbSet<Tbl_Clases> Tbl_Clases { get; set; }

        //public DbSet<Tbl_Inscripciones> Tbl_Inscripciones { get; set; }

    }
}
