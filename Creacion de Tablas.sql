USE [Pruebas]
GO

--Creando tabla de Alumnos
CREATE TABLE [dbo].[Tbl_Alumnos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](15) NOT NULL,
	[Apellido] [varchar](60) NOT NULL,
	[Edad] [int] NOT NULL,
	[Grado] [int] NOT NULL,
 CONSTRAINT [PK_Tbl_Alumnos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--Creando Tabla de clases
CREATE TABLE [dbo].[Tbl_Clases](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](60) NOT NULL,
	[Profesor] [varchar](100) NOT NULL,
	[Aula] [varchar](50) NOT NULL,
	[Grado] [int] NOT NULL,
	[Capacidad] [int] NOT NULL,
 CONSTRAINT [PK_Tbl_Clases] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--Creando tabla de Inscripciones

CREATE TABLE [dbo].[Tbl_Inscripciones](
	[Id_Alumno] [int] NOT NULL,
	[Id_Clase] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[Calificacion] [float] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Tbl_Inscripciones]  WITH CHECK ADD  CONSTRAINT [FK__Tbl_Inscr__Id_Al__3A81B327] FOREIGN KEY([Id_Alumno])
REFERENCES [dbo].[Tbl_Alumnos] ([Id])
GO

ALTER TABLE [dbo].[Tbl_Inscripciones] CHECK CONSTRAINT [FK__Tbl_Inscr__Id_Al__3A81B327]
GO

ALTER TABLE [dbo].[Tbl_Inscripciones]  WITH CHECK ADD  CONSTRAINT [FK__Tbl_Inscr__Id_Cl__3B75D760] FOREIGN KEY([Id_Clase])
REFERENCES [dbo].[Tbl_Clases] ([Id])
GO

ALTER TABLE [dbo].[Tbl_Inscripciones] CHECK CONSTRAINT [FK__Tbl_Inscr__Id_Cl__3B75D760]
GO




